from flask import Flask, jsonify, make_response, redirect, render_template, \
                  render_template_string, request, send_from_directory, \
                  session, url_for
from flask.ext.session import Session
import requests
import html2text
import json
import xlrd
import zipfile
import os
import dr_functions


app = Flask(__name__, static_url_path='')
app.config.from_object('config')
Session(app)


@app.route('/')
def index():
    return redirect(url_for('email_sender'))


# Test Email Sender
@app.route('/email-sender', methods=['GET', 'POST'])
def email_sender():
    # POST request
    if request.method == 'POST':
        recipient = request.form['to'] if request.form['to'] else app.config['ADMIN_EMAIL']
        subject = request.form['subject'] if request.form['subject'] else 'no subject'
        html = request.form['html'] if request.form['html'] else 'empty message'
        text = html2text.html2text(html)
        r = requests.post(
            app.config['MAILGUN_API_URL'],
            auth=("api", app.config['MAILGUN_API_KEY']),
            data={"from": app.config['MAILGUN_FROM_EMAIL'],
                  "to": recipient,
                  "subject": subject,
                  "html": html,
                  'text': text
                  })
        return r.text
    # GET request
    return render_template('email_sender.html')


# Massive Email Builder
@app.route('/email-builder', methods=['GET', 'POST'])
def email_builder():
    # POST request (Dropzone)
    if request.method == 'POST' and request.headers.get('Dropzone') and request.files['file']:
        if os.path.exists('static/emails.zip'):
            os.remove('static/emails.zip')
        try:
            session['builder_vars'] = dr_functions.get_variables(request.files['file'].read())
            return 'OK'
        except:
            return 'Hay errores en el Excel', 400
    # 2nd POST request (HTML insert)
    if request.method == 'POST':
        email_template = request.form['html']
        zip = zipfile.ZipFile('static/emails.zip', mode='a')
        versions = session['builder_vars']
        for version in versions:
            data = versions[version]
            file_name = version + '.html'
            zip.writestr(file_name, render_template_string(email_template, **data))
        zip.close()
        return 'OK'
    # GET request
    if request.method == 'GET':
        return render_template('email_builder.html')


@app.route('/emails.zip')
def zip():
    return send_from_directory(app.static_folder, request.path[1:])


@app.route('/robots.txt')
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404
