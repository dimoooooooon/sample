$(function() {
    // Fade in on page load
    $('.page-content, .page-footer-inner, .selected').removeClass('fade-out');

    // Set cookie for side bar status
    $('.menu-toggler.sidebar-toggler').on('click', function() {
        if ($('body').hasClass('page-sidebar-closed')) {
            Cookies.set('toggledmenu', 'false', { expires: 7 });
        } else {
            Cookies.set('toggledmenu', 'true', { expires: 7 });
        }
    });
});