$( document ).ready(function() {

    // JQuery Validate
    var form = $('#mailgun-form');
    var error = $('.alert-danger', form);
    var success = $('.alert-success', form);
    form.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
            email: {
                required: true,
                email: true
            },
            subject: {
                required: true
            }
        },
        messages: {
            email: {
              required: "Por favor, introduce una dirección de correo!",
              email: "Por favor, introduce una dirección válida!"
            },
            subject: {
              required: "Por favor, introduce el asunto del mensaje!"
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).closest('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight

        },

        success: function (label, element) {
            var icon = $(element).closest('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            submitMailgunForm(); // submit the form
        }
    });
    // Ajax form submit
    var submitMailgunForm = function(){
        var formData = {
            'to'      : $('input[name=email]').val(),
            'subject' : $('input[name=subject]').val(),
            'html'    : myCodeMirror.getValue()
        };
        App.blockUI({
            target: '#portlet',
            animate: true
        });
        $.post('email-sender', formData, function(data){
            if (data.message == 'Queued. Thank you.') {
                data.message = 'Tu mensaje se ha enviado correctamente';
            }
            $('#modal .modal-body').text(data.message);
            App.unblockUI('#portlet');
            $('#modal').modal();
        }, 'json');
        $('#submit').blur();
    };


    // Typeahead | twitter.github.io/typeahead.js
    var substringMatcher = function(strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp( q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };
    var emails = [
        'acastellano@digitalresponse.es',
        'digitalresponse@litmustest.com',
        'dimitri@digitalresponse.es',
        'jpuig@digitalresponse.es',
        'marcgilabert@yahoo.com',
        'mcolomer@digitalresponse.es',
        'mgilabert@digitalresponse.es',
        'mgilabert@outlook.es',
        'msancho@digitalresponse.es',
        'unsubscribe@tous.com'
    ];

    $('#email-typeahead .typeahead').typeahead({
        hint: true,
        highlight: false,
        minLength: 1,
        autoselect: true
    },
    {
        name: 'emails',
        source: substringMatcher(emails)
    });
    // Autocomplete with enter key
    $('#mailgun-form').on('keydown', function(e) {
        if (e.which == 13) {
            e.preventDefault(); // Prevent form submiting
        }
    });
    $('#email').on('keyup', function(e) {
        if(e.which == 13) {
            $('.tt-suggestion:first-child').click();
        }
    });


    // Code editor | codemirror.net
    var myCodeMirror = CodeMirror.fromTextArea(document.getElementById('code-editor'), {
        lineNumbers: true,
        matchBrackets: true,
        styleActiveLine: true,
        selectionPointer: true,
        theme: 'neat',
        mode: {
            name: 'htmlmixed',
            scriptTypes: [
            {
                matches: /\/x-handlebars-template|\/x-mustache/i,
                mode: null
            },
            {
                matches: /(text|application)\/(x-)?vb(a|script)/i,
                mode: "vbscript"
            }]
        }
    });
    var bottomWhiteSpace = $('.portlet-body').height() - $('#mailgun-form').outerHeight(true);
    if ( bottomWhiteSpace > 0 ) {
        myCodeMirror.setSize(null, $('.CodeMirror').outerHeight() + bottomWhiteSpace);
    }

    // Focus on first input
    setTimeout(function(){$('#email').focus();}, 800);

});