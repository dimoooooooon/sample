$( document ).ready(function() {
    Dropzone.options.myAwesomeDropzone = {
        headers: { "Dropzone": "Email builder" },
        dictDefaultMessage: 'Haz click aquí o arrastre y suelte el archivo a subir<span class="note needclick">(Formato de archivos soportados: .xls y .xlsx)</spam>',
        acceptedFiles: ".xls, .xlsx",
        dictInvalidFileType: "Formato incorrecto",
        maxFiles: 1,
        paramName: "file", // The name that will be used to transfer the file
        success: manageResponse,

    };
    function manageResponse(file, response) {
        console.log(response);
        $('#my-awesome-dropzone').fadeOut(500, initCodeEditor);
        $('#step1').removeClass('active');
        $('#step2').addClass('active');
    }
    // Code editor | codemirror.net
    var myCodeMirror;
    function initCodeEditor() {
        myCodeMirror = CodeMirror.fromTextArea(document.getElementById('code-editor'), {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            autofocus: true,
            mode: {
                name: 'jinja2',
                htmlMode: true
            }
        });
        var bottomWhiteSpace = $('.portlet-body').height() - $('#email-builder-html').outerHeight(true) - $('#button-wrapper').outerHeight(true);
        if ( bottomWhiteSpace > 0 ) {
            myCodeMirror.setSize(null, $('.CodeMirror').outerHeight() + bottomWhiteSpace);
        }
        $('.code-editor-wrapper').removeClass('fade-out');
        $('#my-awesome-dropzone').remove();
    }
    // Step 2 | Send form
    function sendHtmlTemplate() {
        var formData = {
            'html': myCodeMirror.getValue()
        };
        App.blockUI({
            target: '#portlet',
            animate: true
        });
        $.post('email-builder', formData, function(response){
            console.log(response);
            $('#step2').removeClass('active');
            $('#step3').addClass('done active');
            App.unblockUI($('#portlet'));
            $('.code-editor-wrapper').remove();
            $('.full-height-content-body').append('<p class="text-center"><a class="btn btn-lg green" href="emails.zip">DESCARGAR</a><p>');
        });
    }
    $('#code-editor-button').on('click', sendHtmlTemplate);
});