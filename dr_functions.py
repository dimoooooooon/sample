import os
import xlrd


# Email builder
def get_variables(excel):
    excel = xlrd.open_workbook(file_contents=excel).sheet_by_index(0)
    variables_dictionary = {}
    for column in range(1, excel.ncols):
        version = str(excel.cell_value(0, column))
        variables_dictionary[version] = {}
        for row in range(1, excel.nrows):
            if isinstance(excel.cell_value(row, column), float):
                variables_dictionary[version][str(excel.cell_value(row, 0))] = str(int(excel.cell_value(row, column)))
            else:
                variables_dictionary[version][str(excel.cell_value(row, 0))] = str(excel.cell_value(row, column))
    return variables_dictionary
